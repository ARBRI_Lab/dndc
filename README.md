## Introduction

Welcome to repository of the modifed DNDC model for cold regions !

Run the 0-dndc.exe for the simulation. 
Data are in the folder of "0-RUN".
Configeration is in dnd file.


<!--## Development-->

<!--- The manuscript is currently under review.-->


## References

*Cui, G., & Wang, J. (2019). Improving the DNDC biogeochemistry model to simulate soil temperature and emissions of nitrous oxide and carbon dioxide in cold regions. Science of The Total Environment.**  

[https://doi.org/10.1016/j.scitotenv.2019.06.054](https://doi.org/10.1016/j.scitotenv.2019.06.054)


*the source code used in this work is available upon request*